import { SocionictoolsPage } from './app.po';

describe('socionictools App', function() {
  let page: SocionictoolsPage;

  beforeEach(() => {
    page = new SocionictoolsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
